from django.apps import AppConfig


class OngAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ong_app'
