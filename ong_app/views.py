from django.shortcuts import render

# Create your views here.

def index(request):
    return render(request, 'ong/accueil.html')

def propos(request):
    return render(request, 'ong/apropos.html')


def activite(request):
    return render(request, 'ong/activite.html')

def contact(request):
    return render(request, 'ong/contact.html')